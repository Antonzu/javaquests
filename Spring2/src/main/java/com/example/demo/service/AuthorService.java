package com.example.demo.service;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;

/**
 * Created by Anton on 01.10.2017.
 */
public interface AuthorService {
    void save(Author author);
    void update(Author author);
    void delete(int id);
    Iterable<Author> findAll();
    Author getById(int id);
    Iterable<Book> findAllBooks(int id);
}
