package com.example.demo.service;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;

/**
 * Created by Anton on 01.10.2017.
 */
public interface BookService {
    void save(Book book);
    void update(Book book);
    void delete(int id);
    Iterable<Book> findAll();
    Book getById(int id);
}
