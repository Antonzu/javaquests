package com.example.demo.service;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.repositoriy.AuthorRepository;
import com.example.demo.repositoriy.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton on 01.10.2017.
 */
@Service
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    BookRepository bookRepository;

    @Override
    public void save(Author author) {
        authorRepository.save(author);
    }

    @Override
    public Iterable<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    public Author getById(int id) {
        return authorRepository.findOne(id);
    }

    public Iterable<Book> findAllBooks(int id) {

        List<Book> allBooks = (ArrayList<Book>)bookRepository.findAll();
        List<Book> authorBooks = new ArrayList<Book>();

        for(Book book : allBooks){
            if(book.getAuthor().getId() == id){
                authorBooks.add(book);
            }
        }

        return authorBooks;
    }

    @Override
    public void update(Author author) {
        authorRepository.save(author);
    }

    @Override
    public void delete(int id) {
        authorRepository.delete(id);
    }
}
