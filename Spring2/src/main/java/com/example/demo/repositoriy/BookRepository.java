package com.example.demo.repositoriy;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Anton on 01.10.2017.
 */
public interface BookRepository extends CrudRepository<Book, Integer> {
}
