package com.example.demo.repositoriy;

import com.example.demo.entity.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Anton on 01.10.2017.
 */
@Repository
public interface AuthorRepository extends CrudRepository<Author, Integer> {
}