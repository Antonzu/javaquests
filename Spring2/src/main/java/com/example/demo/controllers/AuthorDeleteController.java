package com.example.demo.controllers;

import com.example.demo.repositoriy.AuthorRepository;
import com.example.demo.repositoriy.BookRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Anton on 02.10.2017.
 */
@Controller
@RequestMapping("/viewAuthor/delete")
public class AuthorDeleteController {

    @Autowired
    private AuthorService authorService;

    @RequestMapping(method = RequestMethod.POST)
    public String removeAuthor(@RequestParam(value = "authorId") int authorId) {
        authorService.delete(authorId);
        return "redirect:/viewAuthor";
    }
}
