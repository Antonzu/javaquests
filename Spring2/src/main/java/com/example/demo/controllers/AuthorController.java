package com.example.demo.controllers;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.repositoriy.AuthorRepository;
import com.example.demo.repositoriy.BookRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

/**
 * Created by Anton on 01.10.2017.
 */
@Controller
@Component
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "/viewAuthor", method = RequestMethod.GET, produces = "text/html")
    @ResponseBody
    public ModelAndView showHelloWorld() throws Exception{
        List<Author> authors = (ArrayList<Author>)authorService.findAll();
        for(Author author : authors){
            author.setBookList((ArrayList<Book>)authorService.findAllBooks(author.getId()));
        }

        return new ModelAndView("viewAuthor").addObject("authors",authors);
    }

    @RequestMapping(value = "/viewAuthor/save",method = RequestMethod.POST)
    public String createAuthor(@RequestParam String authorName) {
        authorService.save(new Author(authorName));
        return "redirect:/viewAuthor";
    }
}
