package com.example.demo.controllers;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.repositoriy.AuthorRepository;
import com.example.demo.repositoriy.BookRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton on 02.10.2017.
 */
@Controller
public class ViewController {
    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "/viewBook/{id:\\d+}", method = RequestMethod.GET, produces = "text/html")
    @ResponseBody
    public ModelAndView showHelloWorld(@PathVariable int id) throws Exception{
        Author author = authorService.getById(id);
        author.setBookList((ArrayList<Book>)authorService.findAllBooks(author.getId()));

        return new ModelAndView("viewBook").addObject("author",author);
    }
}
