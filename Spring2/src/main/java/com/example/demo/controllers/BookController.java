package com.example.demo.controllers;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.repositoriy.AuthorRepository;
import com.example.demo.repositoriy.BookRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton on 02.10.2017.
 */
@Controller
public class BookController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/createBook/{id:\\d+}",method = RequestMethod.POST)
    public String createBook(@RequestParam("bookName") String bookName, @PathVariable("id") int authorId) {
        bookService.save(new Book(bookName,authorService.getById(authorId)));
        return "redirect:/viewBook/" + authorId;
    }
}
