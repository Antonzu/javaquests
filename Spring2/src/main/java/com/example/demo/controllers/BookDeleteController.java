package com.example.demo.controllers;

import com.example.demo.repositoriy.AuthorRepository;
import com.example.demo.repositoriy.BookRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Anton on 02.10.2017.
 */
@Controller
public class BookDeleteController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/deleteBook/{id:\\d+}", method = RequestMethod.POST)
    public String removeBook(@RequestParam(value = "bookId") int bookId, @PathVariable(value = "id") int authodId) {
        bookService.delete(bookId);
        return "redirect:/viewBook/" + authodId;
    }
}
