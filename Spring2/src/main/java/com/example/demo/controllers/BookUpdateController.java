package com.example.demo.controllers;

import com.example.demo.entity.Author;
import com.example.demo.entity.Book;
import com.example.demo.service.AuthorService;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Anton on 02.10.2017.
 */
@Controller
public class BookUpdateController {
    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/updateBook/{id:\\d+}",method = RequestMethod.POST)
    public String updateAuthor(@RequestParam("bookName") String bookName, @RequestParam(value = "bookId") int bookId, @PathVariable(value = "id") int authorId) {
        Book book = bookService.getById(bookId);
        book.setName(bookName);
        bookService.save(book);

        Book book2 = bookService.getById(bookId);

        return "redirect:/viewBook/" + authorId;
    }
}
