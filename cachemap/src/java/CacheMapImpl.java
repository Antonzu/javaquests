import javax.xml.bind.ValidationEvent;
import java.util.*;

/**
 * Created by Anton on 29.09.2017.
 */
public class CacheMapImpl<KeyType, ValueType> implements CacheMap<KeyType, ValueType> {

    private final long DEFAULT_TIME_TO_LIVE = 1000;//деволтное время жизни
    private long timeToLive;

    Map<KeyType,ValueType> map;
    Map<ValueType,Date> ttlMap;

    public CacheMapImpl(){
        Clock.setTime(DEFAULT_TIME_TO_LIVE);
        timeToLive = Clock.getTime();
        map = new HashMap<KeyType, ValueType>();
        ttlMap = new HashMap<ValueType, Date>();
    }

    public void setTimeToLive(long timeToLive) {
        Clock.setTime(timeToLive);
        this.timeToLive = Clock.getTime();
    }

    public long getTimeToLive() {
        return Clock.getTime();
    }

    public ValueType put(KeyType key, ValueType value) {
            //получаем текущую дату + время жизни
            Date expireDate = new Date(System.currentTimeMillis() + Clock.getTime());
            ttlMap.put(value, expireDate);

            return map.put(key, value);
    }

    public void clearExpired() {
        //здесь удаляются записи у которых истекло время
        Collection<KeyType> keyTypeCollection = map.keySet();
        List<KeyType> keyList = new ArrayList<KeyType>();
        List<ValueType> valueList = new ArrayList<ValueType>();

        for(KeyType keyType : keyTypeCollection){
            ValueType valueType = map.get(keyType);
            if(isExpired(valueType)){
                keyList.add(keyType);
                valueList.add(valueType);
            }
        }

        for(KeyType key : keyList){
            map.remove(key);
        }

        for(ValueType value : valueList){
            ttlMap.remove(value);
        }
    }

    public void clear() {
        map.clear();
        ttlMap.clear();
    }

    public boolean containsKey(Object key) {
        clearExpired();
        return map.containsKey(key);
    }

    public boolean containsValue(Object value) {
        clearExpired();
        return map.containsValue(value);
    }

    public ValueType get(Object key) {
        clearExpired();
        return map.get(key);
    }

    public boolean isEmpty() {
        clearExpired();
        return map.isEmpty();
    }

    public ValueType remove(Object key) {
        return map.remove(key);
    }

    public int size() {
        clearExpired();
        return map.size();
    }

    private boolean isExpired(ValueType key) {
        //здесь идет проверка не истекло ли время
        Date expiryDate = ttlMap.get(key);
        return expiryDate.before(new Date());
    }
}
