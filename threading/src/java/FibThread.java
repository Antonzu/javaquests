/**
 * Created by Anton on 28.09.2017.
 */
public class FibThread implements Runnable {
    FibCalc fib;
    int n;
    int num;

    FibThread(FibCalc fib, int n){
        this.fib = fib;
        this.n = n;
        this.num = 2;
    }

    @Override
    public void run() {
            synchronized (fib){
                System.out.println(Thread.currentThread().getName() + "   " + fib.fib(n));
                fib.clear();
            }
    }
}
