/**
 * Created by Anton on 28.09.2017.
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        FibCalc fib = new FibCalcImpl();
        int n = 40;

        new PerformanceTesterImpl().runPerformanceTest(new FibThread(fib,n),2,5);
    }
}
