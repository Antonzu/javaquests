import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Anton on 28.09.2017.
 */
public class PerformanceTesterImpl implements PerformanceTester {

    @Override
    public PerformanceTestResult runPerformanceTest(Runnable task, int executionCount, int threadPoolSize) throws InterruptedException {

        //выделяю threadPoolSize потоков, executionCount - сколько раз производить вычисления,
        ExecutorService es = Executors.newFixedThreadPool(threadPoolSize);
        for(int i = 0; i < executionCount; i++){
            es.execute(task);
            es.shutdown();
        }


        /*for(int i = 0; i < threadPoolSize; i++)
        {
            Thread t = new Thread(task);
            t.setName("Поток "+ (i+1));
            t.start();
        }*/

        return null;
    }
}
