/**
 * Created by Anton on 28.09.2017.
 */
public class FibCalcImpl implements FibCalc{
    long fib1;
    long fib2;
    int num;

    FibCalcImpl()
    {
        this.fib1 = 1;
        this.fib2 = 1;
        this.num = 2;
    }

    @Override
    public long fib(int n) {

        while(this.num < n){

            long fib3 = this.fib1 + this.fib2;
            this.fib1 = this.fib2;
            this.fib2 = fib3;

            this.num++;
        }

        return this.fib2;
    }

    @Override
    public void clear(){
        this.fib1 = 1;
        this.fib2 = 1;
        this.num = 2;
    }

}
