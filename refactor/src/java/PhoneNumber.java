public class PhoneNumber {
	private String phoneNumber;

	public PhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNumber() {
		return this.phoneNumber;
	}
}