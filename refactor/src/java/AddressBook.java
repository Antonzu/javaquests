import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class AddressBook {

	public boolean hasMobile(String name) {
		if (new AddressDb().findPerson(name).getPhoneNumber().getNumber().startsWith("070")) {
			return true;
		} else {
			return false;
		}		
	}

	static {
		new Checker().start();
	}
	
	public int getSize() {
		return new AddressDb().getAll().size();
	}
	
	/**
	 * Get the given user's mobile phone number,
	 * or null if he doesn't have one.
	 */
	public String getMobile(String name)
	{
		return new AddressDb().findPerson(name).getPhoneNumber().getNumber();
	}
	
	/**
	 * Returns all names in the book. Truncates to the given length.
	 */
	public List getNames(int maxLength) {
		AddressDb db = new AddressDb();
		List<Person> people = db.getAll();
		List names = new LinkedList<String>();
		for (Person person : people) {
			String name = person.getName();
			if (name.length() > maxLength) {
				name = name.substring(0, maxLength);
			}
			names.add(name);
		}

		return names;
	}

	/**
	 * Returns all people who have mobile phone numbers.
	 */
	public List<Person> getList() {
		AddressDb db = new AddressDb();
		List<Person> people = db.getAll();
		Collection f = new LinkedList();
		for (Object person : people) {
			if (((Person)person).getPhoneNumber().getNumber().startsWith("070")) {
				if (people != null) {
					f.add(person);
				}
			}
		}
		return (LinkedList) f;
	}
	
	static class Checker extends Thread {
		long time = System.currentTimeMillis();
		
		public void run() {
			while(System.currentTimeMillis() < time) {
				new AddressBook().getList();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
			
		}
	}
	
}
